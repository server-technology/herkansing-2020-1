﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheaterReservering.Models
{
    public class Reservering
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public bool Bezet { get; set; }
        public int? KlantId { get; set; }
        public Klant Klant { get; set; }
    }
}
