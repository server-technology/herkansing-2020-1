﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Data;
using TheaterReservering.Models;

namespace TheaterReservering
{
    public class DeleteModel : PageModel
    {
        private readonly TheaterReservering.Data.TheaterReserveringContext _context;

        public DeleteModel(TheaterReservering.Data.TheaterReserveringContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Klant Klant { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Klant = await _context.Klant.FirstOrDefaultAsync(m => m.Id == id);

            if (Klant == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Klant = await _context.Klant.FindAsync(id);

            if (Klant != null)
            {
                _context.Klant.Remove(Klant);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
