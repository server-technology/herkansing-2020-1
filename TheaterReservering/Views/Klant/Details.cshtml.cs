﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Data;
using TheaterReservering.Models;

namespace TheaterReservering
{
    public class DetailsModel : PageModel
    {
        private readonly TheaterReservering.Data.TheaterReserveringContext _context;

        public DetailsModel(TheaterReservering.Data.TheaterReserveringContext context)
        {
            _context = context;
        }

        public Klant Klant { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Klant = await _context.Klant.FirstOrDefaultAsync(m => m.Id == id);

            if (Klant == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
