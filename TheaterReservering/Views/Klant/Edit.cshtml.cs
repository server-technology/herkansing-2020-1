﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Data;
using TheaterReservering.Models;

namespace TheaterReservering
{
    public class EditModel : PageModel
    {
        private readonly TheaterReservering.Data.TheaterReserveringContext _context;

        public EditModel(TheaterReservering.Data.TheaterReserveringContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Klant Klant { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Klant = await _context.Klant.FirstOrDefaultAsync(m => m.Id == id);

            if (Klant == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Klant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KlantExists(Klant.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool KlantExists(int id)
        {
            return _context.Klant.Any(e => e.Id == id);
        }
    }
}
