﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheaterReservering.Data;
using TheaterReservering.Models;

namespace TheaterReservering
{
    public class CreateModel : PageModel
    {
        private readonly TheaterReservering.Data.TheaterReserveringContext _context;

        public CreateModel(TheaterReservering.Data.TheaterReserveringContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Klant Klant { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Klant.Add(Klant);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
