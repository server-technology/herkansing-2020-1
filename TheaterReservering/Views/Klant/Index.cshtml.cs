﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Data;
using TheaterReservering.Models;

namespace TheaterReservering
{
    public class IndexModel : PageModel
    {
        private readonly TheaterReservering.Data.TheaterReserveringContext _context;

        public IndexModel(TheaterReservering.Data.TheaterReserveringContext context)
        {
            _context = context;
        }

        public IList<Klant> Klant { get;set; }

        public async Task OnGetAsync()
        {
            Klant = await _context.Klant.ToListAsync();
        }
    }
}
