﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheaterReservering.Models;

namespace TheaterReservering.Data
{
    public static class SeedData
    {
        public static void Initialize(TheaterReserveringContext context)
        {
            context.Database.EnsureCreated();
            AddKlant(context, "Pieter Baan", "Dorpstraat 1", "Nudorp", "pieterbaan@centrum.nl");
            AddKlant(context, "Sjoukje van Leeuwen", "Brink 32", "Dwingeloo", "leeuwerik@live.nl");
            AddKlant(context, "Maria van der Voorst", "Energieweg 23", "Eindhoven", "maria@ikke.com");
            AddKlant(context, "Ad Veenstra", "Amersfoortseweg 12A", "Hilversum", "venie@utc.org");
            
            AddReservering(context, "A1", false, null);
            AddReservering(context, "A2", false, null);
            AddReservering(context, "A3", true, 4);
            AddReservering(context, "A4", true, 4);
            AddReservering(context, "A5", true, 4);
            AddReservering(context, "A6", true, 2);

            AddReservering(context, "B1", false, null);
            AddReservering(context, "B2", false, null);
            AddReservering(context, "B3", false, null);
            AddReservering(context, "B4", true, 4);
            AddReservering(context, "B5", true, 4);
            AddReservering(context, "B6", true, 4);

            AddReservering(context, "C1", false, null);
            AddReservering(context, "C2", false, null);
            AddReservering(context, "C3", false, null);
            AddReservering(context, "C4", false, null);
            AddReservering(context, "C5", false, null);
            AddReservering(context, "C6", false, null);

            AddReservering(context, "D1", false, null);
            AddReservering(context, "D2", false, null);
            AddReservering(context, "D3", false, null);
            AddReservering(context, "D4", true, 3);
            AddReservering(context, "D5", false, null);
            AddReservering(context, "D6", false, null);

            AddReservering(context, "E1", true, 1);
            AddReservering(context, "E2", true, 1);
            AddReservering(context, "E3", false, null);
            AddReservering(context, "E4", false, null);
            AddReservering(context, "E5", false, null);
            AddReservering(context, "E6", false, null);
            context.SaveChanges();
        }

        public static void AddKlant(TheaterReserveringContext context, string naam, string adres, string woonplaats, string email)
        {
            Klant klant = context.Klant.FirstOrDefault(k => k.Naam.Equals(naam));
            if (klant == null) context.Klant.Add(new Klant
            {
                Naam = naam,
                Adres = adres,
                Woonplaats = woonplaats,
                Email = email
            });
        }

        public static void AddReservering(TheaterReserveringContext context, string naam, bool bezet, int? klant)
        {
            Reservering reservering = context.Reservering.FirstOrDefault(r => r.Naam.Equals(naam));
            if (reservering == null) context.Reservering.Add(new Reservering
            {
                Naam = naam,
                Bezet = bezet,
                KlantId = klant
            });
        }
    }
}
