﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Models;

namespace TheaterReservering.Data
{
    public class TheaterReserveringContext : DbContext
    {
        public TheaterReserveringContext (DbContextOptions<TheaterReserveringContext> options)
            : base(options)
        {
        }

        public DbSet<TheaterReservering.Models.Klant> Klant { get; set; }

        public DbSet<TheaterReservering.Models.Reservering> Reservering { get; set; }
    }
}
