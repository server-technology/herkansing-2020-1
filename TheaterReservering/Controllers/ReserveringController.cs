﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Data;
using TheaterReservering.Models;

namespace TheaterReservering.Controllers
{
    public class ReserveringController : Controller
    {
        private readonly TheaterReserveringContext _context;

        public ReserveringController(TheaterReserveringContext context)
        {
            _context = context;
        }

        // GET: Reservering
        public async Task<IActionResult> Index()
        {
            var theaterReserveringContext = _context.Reservering.Include(r => r.Klant);
            return View(await theaterReserveringContext.ToListAsync());
        }

        public async Task<IActionResult> EditWithKlantId(int? id)
        {
            if (id == null)
                return NotFound();

            Klant klant = _context.Klant.FirstOrDefault(k => k.Id == id);

            if (klant == null)
                return NotFound();

            var reserveringen = _context.Reservering.Include(r => r.Klant).OrderBy(r => r.Naam);

            ViewData["Klant"] = $"{klant.Naam}, {klant.Email}, {klant.Woonplaats}";
            ViewData["KlantId"] = klant.Id;

            return View(await reserveringen.ToListAsync());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditWithKlantId(int id, int[] reserveringsIds)
        {
            if (ModelState.IsValid)
            {
                // Verwijder eerst alle reserveringen van Klant
                ICollection<Reservering> reserveringen = _context.Reservering.Where(r => r.KlantId == id).ToList();
                foreach(Reservering r in reserveringen)
                {
                    r.KlantId = null;
                    r.Bezet = false;
                    _context.Update(r);
                    _context.SaveChanges();
                }
                // Voeg alle reserveringen toe die zijn meegegeven
                foreach(int rid in reserveringsIds)
                {
                    Reservering reservering = _context.Reservering.Find(rid);
                    if(reservering != null)
                    {
                        reservering.KlantId = id;
                        reservering.Bezet = true;
                        _context.Update(reservering);
                        _context.SaveChanges();
                    }
                }
                return RedirectToAction(nameof(Index), "Klant");
            }
            return RedirectToAction(nameof(Index), "Klant");
        }

        // GET: Reservering/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservering = await _context.Reservering
                .Include(r => r.Klant)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reservering == null)
            {
                return NotFound();
            }

            return View(reservering);
        }

        // GET: Reservering/Create
        public IActionResult Create()
        {
            ViewData["KlantId"] = new SelectList(_context.Klant, "Id", "Adres");
            return View();
        }

        // POST: Reservering/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naam,Bezet,KlantId")] Reservering reservering)
        {
            if (ModelState.IsValid)
            {
                _context.Add(reservering);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["KlantId"] = new SelectList(_context.Klant, "Id", "Adres", reservering.KlantId);
            return View(reservering);
        }

        // GET: Reservering/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservering = await _context.Reservering.FindAsync(id);
            if (reservering == null)
            {
                return NotFound();
            }
            ViewData["KlantId"] = new SelectList(_context.Klant, "Id", "Adres", reservering.KlantId);
            return View(reservering);
        }

        // POST: Reservering/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naam,Bezet,KlantId")] Reservering reservering)
        {
            if (id != reservering.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reservering);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReserveringExists(reservering.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["KlantId"] = new SelectList(_context.Klant, "Id", "Adres", reservering.KlantId);
            return View(reservering);
        }

        // GET: Reservering/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reservering = await _context.Reservering
                .Include(r => r.Klant)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reservering == null)
            {
                return NotFound();
            }

            return View(reservering);
        }

        // POST: Reservering/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reservering = await _context.Reservering.FindAsync(id);
            _context.Reservering.Remove(reservering);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReserveringExists(int id)
        {
            return _context.Reservering.Any(e => e.Id == id);
        }
    }
}
