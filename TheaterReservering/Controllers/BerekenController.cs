﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TheaterReservering.Data;
using TheaterReservering.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TheaterReservering.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BerekenController : ControllerBase
    {
        private readonly TheaterReserveringContext _context;

        public BerekenController(TheaterReserveringContext context)
        {
            _context = context;
        }

        // GET api/<BerekenController>/5
        [HttpGet("{id}")]
        public int Get(int id)
        {
            if (id == null)
                return 0;
            Klant klant = _context.Klant.Include(k => k.Reserveringen).FirstOrDefault(k => k.Id == id);

            if (klant == null)
                return 0;
            int bedrag = 0;
            foreach(Reservering res in klant.Reserveringen)
            {
                if (res.Naam.StartsWith("A")) bedrag += 40;
                else if (res.Naam.StartsWith("B")) bedrag += 35;
                else if (res.Naam.StartsWith("C")) bedrag += 30;
                else if (res.Naam.StartsWith("D")) bedrag += 25;
                else if (res.Naam.StartsWith("E")) bedrag += 20;
                if (res.Naam.EndsWith("3") || res.Naam.EndsWith("4")) bedrag += 5;
            }

            return bedrag;
        }
    }
}
